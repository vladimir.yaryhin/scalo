import {SCALE_NAMES} from "./scales.constants";
import {MOOD} from "../../components/octaves/assets/octaves.constants";

export interface IScale {
    name: SCALE_NAMES;
    mood: MOOD;
    label: string;
    activeKeys: number[];
}

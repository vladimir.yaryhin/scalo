import {MOOD} from "../../components/octaves/assets/octaves.constants";
import {IScale} from "./scales.interface";

export enum SCALE_NAMES {
  C_MINOR,
  C_MAJOR,
  C_SHARP_MINOR,
  C_SHARP_MAJOR,
  D_MINOR,
  D_MAJOR,
  D_SHARP_MINOR,
  D_SHARP_MAJOR,
  E_MINOR,
  E_MAJOR,
  F_MINOR,
  F_MAJOR,
  F_SHARP_MINOR,
  F_SHARP_MAJOR,
  G_MINOR,
  G_MAJOR,
  G_SHARP_MINOR,
  G_SHARP_MAJOR,
  A_MINOR,
  A_MAJOR,
  A_SHARP_MINOR,
  A_SHARP_MAJOR,
  B_MINOR,
  B_MAJOR,
}

export const SCALES: IScale[] = [
  {
    name: SCALE_NAMES.C_MAJOR,
    label: 'Cmaj',
    mood: MOOD.MAJOR,
    activeKeys: [1, 3, 5, 6, 8, 10, 12]
  },
  {
    name: SCALE_NAMES.C_MINOR,
    label: 'Cmin',
    mood: MOOD.MINOR,
    activeKeys: [1, 3, 4, 6, 8, 9, 11]
  },
  {
    name: SCALE_NAMES.C_SHARP_MAJOR,
    label: 'C#maj',
    mood: MOOD.MAJOR,
    activeKeys: [2, 4, 6, 7, 9, 11, 13]
  },
  {
    name: SCALE_NAMES.C_SHARP_MINOR,
    label: 'C#min',
    mood: MOOD.MINOR,
    activeKeys: [2, 4, 5, 7, 9, 10, 12]
  },
  {
    name: SCALE_NAMES.D_MAJOR,
    label: 'Dmaj',
    mood: MOOD.MAJOR,
    activeKeys: [3, 5, 7, 8, 10, 12, 14]
  },
  {
    name: SCALE_NAMES.D_MINOR,
    label: 'Dmin',
    mood: MOOD.MINOR,
    activeKeys: [3, 5, 6, 8, 10, 11, 13]
  },
  {
    name: SCALE_NAMES.D_SHARP_MAJOR,
    label: 'D#maj',
    mood: MOOD.MAJOR,
    activeKeys: [4, 6, 8, 9, 11, 13, 15]
  },
  {
    name: SCALE_NAMES.D_SHARP_MINOR,
    label: 'D#min',
    mood: MOOD.MINOR,
    activeKeys: [4, 6, 7, 9, 11, 12, 14]
  },
  {
    name: SCALE_NAMES.E_MAJOR,
    label: 'Emaj',
    mood: MOOD.MAJOR,
    activeKeys: [5, 7, 9, 10, 12, 14, 16]
  },
  {
    name: SCALE_NAMES.E_MINOR,
    label: 'Emin',
    mood: MOOD.MINOR,
    activeKeys: [5, 7, 8, 10, 12, 13, 15]
  },
  {
    name: SCALE_NAMES.F_MAJOR,
    label: 'Fmaj',
    mood: MOOD.MAJOR,
    activeKeys: [6, 8, 10, 11, 13, 15, 17]
  },
  {
    name: SCALE_NAMES.F_MINOR,
    label: 'Fmin',
    mood: MOOD.MINOR,
    activeKeys: [6, 8, 9, 11, 13, 14, 16]
  },
  {
    name: SCALE_NAMES.F_SHARP_MAJOR,
    label: 'F#maj',
    mood: MOOD.MAJOR,
    activeKeys: [7, 9, 11, 12, 14, 16, 18]
  },
  {
    name: SCALE_NAMES.F_SHARP_MINOR,
    label: 'F#min',
    mood: MOOD.MINOR,
    activeKeys: [7, 9, 10, 12, 14, 15, 17]
  },
  {
    name: SCALE_NAMES.G_MAJOR,
    label: 'Gmaj',
    mood: MOOD.MAJOR,
    activeKeys: [8, 10, 12, 13, 15, 17, 19]
  },
  {
    name: SCALE_NAMES.G_MINOR,
    label: 'Gmin',
    mood: MOOD.MINOR,
    activeKeys: [8, 10, 11, 13, 15, 16, 18]
  },
  {
    name: SCALE_NAMES.G_SHARP_MAJOR,
    label: 'G#maj',
    mood: MOOD.MAJOR,
    activeKeys: [9, 11, 13, 14, 16, 18, 20]
  },
  {
    name: SCALE_NAMES.G_SHARP_MINOR,
    label: 'G#min',
    mood: MOOD.MINOR,
    activeKeys: [9, 11, 12, 14, 16, 17, 19]
  },
  {
    name: SCALE_NAMES.A_MAJOR,
    label: 'Amaj',
    mood: MOOD.MAJOR,
    activeKeys: [10, 12, 14, 15, 17, 19, 21]
  },
  {
    name: SCALE_NAMES.A_MINOR,
    label: 'Amin',
    mood: MOOD.MINOR,
    activeKeys: [10, 12, 13, 15, 17, 18, 20]
  },
  {
    name: SCALE_NAMES.A_SHARP_MAJOR,
    label: 'A#maj',
    mood: MOOD.MAJOR,
    activeKeys: [11, 13, 15, 16, 18, 20, 22]
  },
  {
    name: SCALE_NAMES.A_SHARP_MINOR,
    label: 'A#min',
    mood: MOOD.MINOR,
    activeKeys: [11, 13, 14, 16, 18, 19, 21]
  },
  {
    name: SCALE_NAMES.B_MAJOR,
    label: 'Bmaj',
    mood: MOOD.MAJOR,
    activeKeys: [12, 14, 16, 17, 19, 21, 23]
  },
  {
    name: SCALE_NAMES.B_MINOR,
    label: 'Bmin',
    mood: MOOD.MINOR,
    activeKeys: [12, 14, 15, 17, 19, 20, 22]
  },
];

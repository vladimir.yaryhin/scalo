import { Component, OnInit } from '@angular/core';
import {IOctaveOptions} from "../../components/octaves/assets/octaves.interface";
import {SCALES} from "./scales.constants";
import {FormControl} from "@angular/forms";
import {Observable} from "rxjs";
import {OCTAVE_SIZES} from "../../components/octaves/assets/octaves.constants";
import {MatDialog} from "@angular/material/dialog";
import {map, startWith} from "rxjs/operators";
import {BigDaddyOctaveComponent} from "../../components/big-daddy-octave/big-daddy-octave.component";
import {IScale} from "./scales.interface";

@Component({
  selector: 'app-scales',
  templateUrl: './scales.component.html',
  styleUrls: ['./scales.component.scss']
})
export class ScalesComponent implements OnInit {
  scales: IScale[] = SCALES;
  searchControl: FormControl = new FormControl();
  options: string[];
  filteredOptions: Observable<string[]>;
  octaveConfigs: IOctaveOptions = {
    size: OCTAVE_SIZES.SM,
    isHoverOn: true
  };

  constructor(public dialog: MatDialog) {}

  ngOnInit() {
    this.options = this.scales.map((scale: IScale) => scale.label);

    this.filteredOptions = this.searchControl.valueChanges
        .pipe(
            startWith(''),
            map(value => this._filter(value))
        );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    if (value !== '') {
      this.scales = SCALES.filter((scale: IScale) => scale.label.toLowerCase().includes(filterValue))
    } else {
      this.scales = SCALES;
    }
    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  openDialog(scale: IScale) {
    const dialogRef = this.dialog.open(BigDaddyOctaveComponent, {
      data: scale
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
      // console.log(result);
    });
  }

}

import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {IOctaveOptions} from "../octaves/assets/octaves.interface";
import {OCTAVE_SIZES} from "../octaves/assets/octaves.constants";
import {IScale} from "../../pages/scales/scales.interface";
import {ChordGeneratorService} from "../octaves/chord-generator.service";
import {IChord} from "../chords/chords.interface";

@Component({
  selector: 'app-big-daddy-octave',
  templateUrl: './big-daddy-octave.component.html',
  styleUrls: ['./big-daddy-octave.component.scss']
})
export class BigDaddyOctaveComponent implements OnInit {
  octaveConfigs: IOctaveOptions = {
    size: OCTAVE_SIZES.MD,
    isHoverOn: false
  };
  chordConfigs: IOctaveOptions = {
    size: OCTAVE_SIZES.SM,
    isHoverOn: false
  }
  currentChords: Map<string, IChord[]>;
  practiceChords: IChord[] = [];

  constructor(
    public dialogRef: MatDialogRef<BigDaddyOctaveComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IScale,
    public chordGeneratorService: ChordGeneratorService) {}

  ngOnInit() {
    this.currentChords = this.chordGeneratorService.getChordsByScale(this.data);
  }

  getCurrentChordsRoots(): string[] {
    return Array.from(this.currentChords.keys());
  }

  close(): void {
    this.dialogRef.close();
  }

  addToPractice(chord: IChord, event): void {
    event.stopPropagation();

    this.practiceChords.push(chord);
  }

  removeFromPractice(chord: IChord, event): void {
    event.stopPropagation();

    this.practiceChords = this.practiceChords.filter(practiceChord => practiceChord.name !== chord.name);
  }
}

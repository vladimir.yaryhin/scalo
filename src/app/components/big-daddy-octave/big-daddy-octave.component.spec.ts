import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BigDaddyOctaveComponent } from './big-daddy-octave.component';

describe('BigDaddyOctaveComponent', () => {
  let component: BigDaddyOctaveComponent;
  let fixture: ComponentFixture<BigDaddyOctaveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BigDaddyOctaveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BigDaddyOctaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {KEY_BUTTON_TYPE, KEY_NAMES, OCTAVE_SIZES} from "./octaves.constants";

export interface IKeyButton {
  id: number;
  name: KEY_NAMES;
  type: KEY_BUTTON_TYPE;
  isActive: boolean;
}

export interface IOctaveOptions {
  size: OCTAVE_SIZES;
  isHoverOn: boolean;
}

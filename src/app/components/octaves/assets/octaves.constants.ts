import {IKeyButton} from "./octaves.interface";

export enum OCTAVE_SIZES {
  LG,
  MD,
  SM
}

export enum KEY_BUTTON_TYPE {
  KEY_BUTTON_WHITE,
  KEY_BUTTON_BLACK,
  KEY_BUTTON_EMPTY
}

export enum MOOD {
  MINOR = 'min',
  MAJOR = 'maj'
}

export enum KEY_NAMES {
  C= 'C',
  C_SHARP = 'C#',
  D = 'D',
  D_SHARP = 'D#',
  E = 'E',
  F = 'F',
  F_SHARP = 'F#',
  G = 'G',
  G_SHARP = 'G#',
  A = 'A',
  A_SHARP = 'A#',
  B = 'B'
}

export const EMPTY_KEY: IKeyButton = {
  id: 0,
  name: null,
  type: KEY_BUTTON_TYPE.KEY_BUTTON_EMPTY,
  isActive: false
};

export const OCTAVES: IKeyButton[] = [
  {
    id: 1,
    name: KEY_NAMES.C,
    type: KEY_BUTTON_TYPE.KEY_BUTTON_WHITE,
    isActive: false
  },
  {
    id: 2,
    name: KEY_NAMES.C_SHARP,
    type: KEY_BUTTON_TYPE.KEY_BUTTON_BLACK,
    isActive: false
  },
  {
    id: 3,
    name: KEY_NAMES.D,
    type: KEY_BUTTON_TYPE.KEY_BUTTON_WHITE,
    isActive: false
  },
  {
    id: 4,
    name: KEY_NAMES.D_SHARP,
    type: KEY_BUTTON_TYPE.KEY_BUTTON_BLACK,
    isActive: false
  },
  {
    id: 5,
    name: KEY_NAMES.E,
    type: KEY_BUTTON_TYPE.KEY_BUTTON_WHITE,
    isActive: false
  },
  {
    id: 6,
    name: KEY_NAMES.F,
    type: KEY_BUTTON_TYPE.KEY_BUTTON_WHITE,
    isActive: false
  },
  {
    id: 7,
    name: KEY_NAMES.F_SHARP,
    type: KEY_BUTTON_TYPE.KEY_BUTTON_BLACK,
    isActive: false
  },
  {
    id: 8,
    name: KEY_NAMES.G,
    type: KEY_BUTTON_TYPE.KEY_BUTTON_WHITE,
    isActive: false
  },
  {
    id: 9,
    name: KEY_NAMES.G_SHARP,
    type: KEY_BUTTON_TYPE.KEY_BUTTON_BLACK,
    isActive: false
  },
  {
    id: 10,
    name: KEY_NAMES.A,
    type: KEY_BUTTON_TYPE.KEY_BUTTON_WHITE,
    isActive: false
  },
  {
    id: 11,
    name: KEY_NAMES.A_SHARP,
    type: KEY_BUTTON_TYPE.KEY_BUTTON_BLACK,
    isActive: false
  },
  {
    id: 12,
    name: KEY_NAMES.B,
    type: KEY_BUTTON_TYPE.KEY_BUTTON_WHITE,
    isActive: false
  },
  {
    id: 13,
    name: KEY_NAMES.C,
    type: KEY_BUTTON_TYPE.KEY_BUTTON_WHITE,
    isActive: false
  },
  {
    id: 14,
    name: KEY_NAMES.C_SHARP,
    type: KEY_BUTTON_TYPE.KEY_BUTTON_BLACK,
    isActive: false
  },
  {
    id: 15,
    name: KEY_NAMES.D,
    type: KEY_BUTTON_TYPE.KEY_BUTTON_WHITE,
    isActive: false
  },
  {
    id: 16,
    name: KEY_NAMES.D_SHARP,
    type: KEY_BUTTON_TYPE.KEY_BUTTON_BLACK,
    isActive: false
  },
  {
    id: 17,
    name: KEY_NAMES.E,
    type: KEY_BUTTON_TYPE.KEY_BUTTON_WHITE,
    isActive: false
  },
  {
    id: 18,
    name: KEY_NAMES.F,
    type: KEY_BUTTON_TYPE.KEY_BUTTON_WHITE,
    isActive: false
  },
  {
    id: 19,
    name: KEY_NAMES.F_SHARP,
    type: KEY_BUTTON_TYPE.KEY_BUTTON_BLACK,
    isActive: false
  },
  {
    id: 20,
    name: KEY_NAMES.G,
    type: KEY_BUTTON_TYPE.KEY_BUTTON_WHITE,
    isActive: false
  },
  {
    id: 21,
    name: KEY_NAMES.G_SHARP,
    type: KEY_BUTTON_TYPE.KEY_BUTTON_BLACK,
    isActive: false
  },
  {
    id: 22,
    name: KEY_NAMES.A,
    type: KEY_BUTTON_TYPE.KEY_BUTTON_WHITE,
    isActive: false
  },
  {
    id: 23,
    name: KEY_NAMES.A_SHARP,
    type: KEY_BUTTON_TYPE.KEY_BUTTON_BLACK,
    isActive: false
  },
  {
    id: 24,
    name: KEY_NAMES.B,
    type: KEY_BUTTON_TYPE.KEY_BUTTON_WHITE,
    isActive: false
  }
];

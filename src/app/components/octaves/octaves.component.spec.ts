import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OctavesComponent } from './octaves.component';

describe('OctaveComponent', () => {
  let component: OctavesComponent;
  let fixture: ComponentFixture<OctavesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OctavesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OctavesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {IKeyButton, IOctaveOptions} from "./assets/octaves.interface";
import {EMPTY_KEY, KEY_BUTTON_TYPE, KEY_NAMES, OCTAVE_SIZES, OCTAVES} from "./assets/octaves.constants";

@Component({
  selector: 'app-octave',
  templateUrl: './octaves.component.html',
  styleUrls: ['./octaves.component.scss']
})
export class OctavesComponent implements OnInit, OnChanges {
  @Input() scale: number[];
  @Input() options: IOctaveOptions;
  octave: IKeyButton[] = OCTAVES;
  octaveSizes = OCTAVE_SIZES;
  buttonTypes = KEY_BUTTON_TYPE;
  blackKeysRow: IKeyButton[];
  currentSize: OCTAVE_SIZES;

  constructor() { }

  ngOnInit(): void {

    this.blackKeysRow = this.octave.map((key: IKeyButton) => {
      if (key.type === this.buttonTypes.KEY_BUTTON_WHITE) {
        return EMPTY_KEY;
      } else {
        return key;
      }
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.scale) {
      this.octave = this.octave.map((keyButton: IKeyButton) => {
        if (changes.scale.currentValue.indexOf(keyButton.id) !== -1) {
            return {
              ...keyButton,
              isActive: true
            };
        } else {
          return keyButton;
        }
      });
    }

    if (changes.options) {
      this.currentSize = changes.options.currentValue.size;
    }
  }

  getKeyName(keyId: number): KEY_NAMES {
    return this.octave.find(key => {
      return key.id === keyId;
    }).name;
  }

  getBlackKeyName(keyId: number): KEY_NAMES {
    return this.blackKeysRow.find(key => {
      return key.id === keyId;
    }).name;
  }

}

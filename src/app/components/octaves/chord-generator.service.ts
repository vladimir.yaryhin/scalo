import {Injectable} from "@angular/core";
import {IScale} from "../../pages/scales/scales.interface";
import {IChord} from "../chords/chords.interface";
import {IKeyButton} from "./assets/octaves.interface";
import {KEY_NAMES, MOOD, OCTAVES} from "./assets/octaves.constants";
import {CHORD_TYPES} from "../chords/chords.constants";

interface IChordConfig {
    rootName: KEY_NAMES,
    rootId: number,
    type: CHORD_TYPES,
    mood: MOOD,
    pattern: number[],
    label?: string
}

@Injectable({providedIn: 'root'})
export class ChordGeneratorService {
    readonly MAJOR_FIFTH_CHORD_C_PATTERN = [1,5,8];
    readonly MINOR_FIFTH_CHORD_C_PATTERN = [1,4,8];
    readonly MAJOR_SEVENTH_CHORD_C_PATTERN = [1,5,8,12];
    readonly MINOR_SEVENTH_CHORD_C_PATTERN = [1,4,8,11];
    readonly MAJOR_NINTH_CHORD_C_PATTERN = [1,5,8,12,15];
    readonly MINOR_NINTH_CHORD_C_PATTERN = [1,4,8,11,15];

    readonly C_ROOT = 1;
    readonly MAX_KEY_NUMBERS = 24;

    getChordsByScale(scale: IScale): Map<string, IChord[]> {
        let scaleChords: Map<string, IChord[]> = new Map();

        scale.activeKeys.forEach((keyId: number) => {
            const keyName: KEY_NAMES = OCTAVES.find((key: IKeyButton) => key.id === keyId).name;

            let majorFifthChordConfig: IChordConfig = {
                rootName: keyName,
                rootId: keyId,
                type: CHORD_TYPES.FIFTH,
                mood: MOOD.MAJOR,
                pattern: this.MAJOR_FIFTH_CHORD_C_PATTERN
            };

            let minorFifthChordConfig: IChordConfig = {
                rootName: keyName,
                rootId: keyId,
                type: CHORD_TYPES.FIFTH,
                mood: MOOD.MINOR,
                pattern: this.MINOR_FIFTH_CHORD_C_PATTERN
            };

            let majorSeventhChordConfig: IChordConfig = {
                rootName: keyName,
                rootId: keyId,
                type: CHORD_TYPES.SEVENTH,
                mood: MOOD.MAJOR,
                pattern: this.MAJOR_SEVENTH_CHORD_C_PATTERN,
                label: '7'
            };

            let minorSeventhChordConfig: IChordConfig = {
                rootName: keyName,
                rootId: keyId,
                type: CHORD_TYPES.SEVENTH,
                mood: MOOD.MINOR,
                pattern: this.MINOR_SEVENTH_CHORD_C_PATTERN,
                label: '7'
            };

            let majorNinthChordConfig: IChordConfig = {
                rootName: keyName,
                rootId: keyId,
                type: CHORD_TYPES.NINTH,
                mood: MOOD.MAJOR,
                pattern: this.MAJOR_NINTH_CHORD_C_PATTERN,
                label: '9'
            };

            let minorNinthChordConfig: IChordConfig = {
                rootName: keyName,
                rootId: keyId,
                type: CHORD_TYPES.NINTH,
                mood: MOOD.MINOR,
                pattern: this.MINOR_NINTH_CHORD_C_PATTERN,
                label: '9'
            };

            scaleChords.set(`${keyName}`, [
                this.createChord(majorFifthChordConfig),
                this.createChord(minorFifthChordConfig)
            ]);

            scaleChords.set(`${keyName}7`, [
                this.createChord(majorSeventhChordConfig),
                this.createChord(minorSeventhChordConfig)
            ]);

            scaleChords.set(`${keyName}9`, [
                this.createChord(majorNinthChordConfig),
                this.createChord(minorNinthChordConfig)
            ]);
        });

        return scaleChords;
    }

    createChord(chordConfig: IChordConfig): IChord {
        let chord: IChord = {
            name: `${chordConfig.rootName}${chordConfig.mood}${chordConfig.label ? chordConfig.label : ''}`,
            mood: chordConfig.mood,
            chordRoot: chordConfig.rootId,
            activeKeys: [],
            type: chordConfig.type
        }

        const rootDiff = chordConfig.rootId - this.C_ROOT;

        chord.activeKeys = chordConfig.pattern.map((currentKey) => {
            let currentKeyId = currentKey + rootDiff;
            return currentKeyId > this.MAX_KEY_NUMBERS ? currentKeyId - this.MAX_KEY_NUMBERS : currentKeyId;
        });

        return chord;
    }
}

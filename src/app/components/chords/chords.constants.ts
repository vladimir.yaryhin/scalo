export enum CHORD_TYPES {
    FIFTH,
    SEVENTH,
    NINTH
}

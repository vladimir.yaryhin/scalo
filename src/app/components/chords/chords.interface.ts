import {MOOD} from "../octaves/assets/octaves.constants";
import {CHORD_TYPES} from "./chords.constants";

export interface IChord {
    type: CHORD_TYPES;
    name: string;
    mood: MOOD;
    activeKeys: number[];
    chordRoot: number;
}
